@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <h2>Выборка результатов из Google</h2>
        </div>
        <div class="col-md-6"></div>
    </div>
    <div>
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" href="/">Главная</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/search">История парсинга</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Настройки парсинга</a>
            </li>
        </ul>
    </div>
    <div>
        <div class="alert alert-success success" role="alert">
            Домен {{ $domain ?? '' }} по ключевому слову находится на {{ $num ?? '' }} позиции выдачи Google
        </div>
    </div>
    <br>
    <div>
        <div class="alert alert-light" role="alert">
            <h4>Введите домен и ключевое слово.</h4>
        </div>
    </div>
    <div class="container">
        <form role="form" method="post" action="{{ url('/search') }}">
            {{ csrf_field() }}
            <div class="row alert alert-secondary">
                <div class="col-md-6">
                    <div class="alert alert-light" role="alert">
                        <label for="domen">Домен</label>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <input name="domen" id="domen" class="form-control form-control-lg" type="text" placeholder="">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="alert alert-light" role="alert">
                        <label for="keyword">Ключевое слово</label>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <input name="keyword" id="keyword" class="form-control form-control-lg" type="text" placeholder="">
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary mb-2">Загрузить</button>
        </form>
    </div>
</div>
@endsection
