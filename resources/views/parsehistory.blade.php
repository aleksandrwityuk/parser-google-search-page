@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2>История парсинга</h2>
            </div>
            <div class="col-md-6"></div>
        </div>
        <div>
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link" href="/">Главная</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="/search">История парсинга</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Настройки парсинга</a>
                </li>
            </ul>
        </div>
        <div>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">Доменное имя</th>
                    <th scope="col">Ключевое слово</th>
                    <th scope="col">Позиция в Google</th>
                    <th scope="col">Дата выборки</th>
                </tr>
                </thead>
                <tbody>
                @if ( $domains )
                    @foreach( $domains as $dom )
                        <tr>
                            <th>{{ $dom->id ?? '' }}</th>
                            <td>{{ $dom->name ?? '' }}</td>
                            <td>{{ $keyword ?? ''  }}</td>
                            <td>
                                @if ( strpos($domain, $dom->name ) !== false)
                                    {{ $num ?? ' ' }}
                                @else
                                    {{  'Not found' }}
                                @endif
                            </td>
                            <td>{{ $dom->date ?? '' }}</td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            {{ $domains->links() }}
        </div>
    </div>
@endsection
