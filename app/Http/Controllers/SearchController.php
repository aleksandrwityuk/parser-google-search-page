<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use test\Mockery\MockingVariadicArgumentsTest;
use function PHPUnit\Framework\StaticAnalysis\HappyPath\AssertNull\consume;

class SearchController extends Controller
{

    public function index (  Request $request )
    {

        $domain = $request->session()->get('domain', '');
        $num = $request->session()->get('num', '');

        return view('home', ['domain' =>  $domain, 'num' => $num]);
    }

    public function search( Request $request )
    {
        $domain = $request->input('domen');
        $request->session()->put('domain', $domain);
        $keyword = $request->input('keyword');
        $request->session()->put('keyword', $keyword);

        $kk = 100;

        $url="https://www.google.com/search?num=".$kk."&q=".$keyword;

        $result = $this->parse( $url );

        $num = $this -> getNum ( $result, $domain );
        $request->session()->put('num', $num);

        return view('home', ['domain' =>  $domain, 'num' => $num]);

    }

    public function getResult( Request $request )
    {

        $domain = $request->session()->get('domain', 'undefined');
        $keyword = $request->session()->get('keyword', 'undefined');

        //$domains = DB::select( 'select * from domains ');
        $domains = DB::table('domains')->paginate(3);
        $domnames = array();
        foreach ( $domains as $dom )
        {
            $domnames[] = $dom->name;
        }

        $num = $this->getNum ( $domnames, $domain );

        return view('parsehistory')->with( ['domains' => $domains, 'domain' => $domain,  'num' => $num, 'keyword' => $keyword] );
    }


    private function parse($url)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookies.txt');
        curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookies.txt');
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5');
        curl_setopt ($ch , CURLOPT_REFERER , 'https://google.com/');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,20);
        @curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);


        $data_url = $data_title = $data_description = curl_exec($ch);
        curl_close($ch);

        preg_match_all('|<span class="fYyStc">http([s]?)://(.*)</span>|U',$data_url,$matches_url);

        $url_array = $matches_url[2];
        $new_url_array = array();

        foreach ( $url_array as $url )
        {
            $url = explode(' ', $url);
            $new_url_array[] = $url[0];
            $date = date('Y-m-d H:i:s');
            DB::table('domains')->insert( ['name' => $url[0], 'date' => $date] );

        }

        return $new_url_array;
    }

    private function getNum ( $result, $domain )
    {
        $i = '';
        if ( $domain !== '' )
        {
            foreach ( $result as $res )
            {
                if ( strpos($domain, $res) !== false )
                {
                    $i++;
                    break;
                }
                $i++;
            }
        }

        return $i;
    }

}
